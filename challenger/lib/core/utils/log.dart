bool isDebug = true;

class Log {
  static void d({String tag, String msg}) {
    if (isDebug) {
      print("$tag: $msg");
    }
  }
}
