enum ApiMethod { get, post, put, delete }

abstract class IApiInput {
  String endPoint;
  dynamic body;
  ApiMethod method;
  Map<String, String> header;
  Map<String, String> params;

  IApiInput(this.endPoint, this.method, this.header, this.body, this.params);
}

class ApiInput extends IApiInput {
  ApiInput(String endPoint, ApiMethod method, Map<String, String> header,
      Map<String, dynamic> body, Map<String, String> param)
      : super(endPoint, method, header, body, param);
}
