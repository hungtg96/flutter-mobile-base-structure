import 'package:challenger/core/network/network_status.dart';
import 'package:challenger/data/entity/token_entity.dart';
import 'base_api.dart';

import 'interface_api.dart';

class AuthenApiImpl extends AuthenApi with BaseApi {
  NetworkStatus networkStatus;
  AuthenApiImpl({this.networkStatus});

  @override
  Future<TokenEntity> login(String username, String password) async {
    var params = {'ten_truy_cap': username, 'mat_khau': password};
    var header = await buildHeader();
    final json = await ApiConnection(apiCofig, networkStatus: networkStatus)
        .execute(
            new ApiInput('/auth/signin', ApiMethod.post, header, params, null));
    return TokenEntity.fromJson(json);
  }
}
