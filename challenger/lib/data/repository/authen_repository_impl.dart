import 'package:challenger/data/datasource/remote/interface_api.dart';
import 'package:challenger/data/datasource/authen_cache.dart';
import 'package:challenger/domain/model/token_model.dart';
import 'package:challenger/domain/repository/authen_repository.dart';

class AuthenticationRepositoryImpl implements AuthenticationRepository {
  AuthenApi _authenApi;
  AuthenCache _authenCache;

  AuthenticationRepositoryImpl(this._authenApi, this._authenCache)
      : assert(_authenApi != null),
        assert(_authenCache != null);

  @override
  Future<TokenModel> login(String username, String password) async {
    assert((username?.isNotEmpty ?? false), 'username is null');
    assert((password?.isNotEmpty ?? false), 'password is null');
    final token = await _authenApi.login(username, password);
    _authenCache.putToken(token);
    return token;
  }

  @override
  Future<TokenModel> getCachedToken() async {
    var token = await _authenCache.getCachedToken();
    return token;
  }

  @override
  logout() async {
    _authenCache.removeToken();
  }
}
