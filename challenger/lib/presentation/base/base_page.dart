import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:challenger/app_injector.dart';
import '../app/application_bloc.dart';
import 'base_bloc.dart';
import 'base_event.dart';
import 'base_page_mixin.dart';
import 'base_router.dart';
import 'base_state.dart';

abstract class BasePage<T extends BaseBloc<dynamic, dynamic>>
    extends StatefulWidget {
  BasePage({Key key, this.bloc, this.router}) : super(key: key);

  final BaseBloc bloc;
  final BaseRouter router;
}

abstract class BasePageState<
    T extends BaseBloc<BaseEvent, BaseState>,
    P extends BasePage,
    R extends BaseRouter> extends State<P> with BasePageMixin {
  BaseBloc bloc;
  BuildContext subContext;
  BaseRouter router;
  ApplicationBloc applicationBloc;
  StreamSubscription<BaseEvent> appStreamSubscription;

  @override
  void didChangeDependencies() {
    bloc?.dispatchEvent(PageDidChangeDependenciesEvent(context: context));
    super.didChangeDependencies();
  }

  @override
  void initState() {
    bloc = widget.bloc ?? injector.get<T>();
    bloc?.dispatchEvent(PageInitStateEvent(context: context));
    router = widget.router ?? injector.get<R>();
    applicationBloc = BlocProvider.of<ApplicationBloc>(context);
    appStreamSubscription = applicationBloc.appEventStream.listen((event) {
      bloc.dispatchEvent(event);
    });
    super.initState();
  }

  Widget buildBody(BuildContext context, BaseBloc bloc);

  void stateListenerCallBack(BaseState state) {
    if (state is ErrorState) {
      print(' stateListenerCallBack  ${state.messageError}');
      showAlert(context: context, message: state.messageError);
    }
  }

  _onResult(dynamic res) {
    if (res != null) {
      bloc?.dispatchEvent(OnResultEvent(result: res));
    }
  }

  @override
  Widget build(BuildContext context) {
    bloc?.dispatchEvent(PageBuildEvent(context: context));
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BlocProvider(
        create: (context) => bloc,
        child: BlocListener<BaseBloc, BaseState>(listener: (bloc, state) async {
          stateListenerCallBack(state);
          final res = await router?.onNavigate(context: context, state: state);
          _onResult(res);
        }, child: LayoutBuilder(builder: (sub, _) {
          subContext = sub;
          return buildBody(subContext, bloc);
        })),
      ),
    );
  }

  @override
  dispose() {
    appStreamSubscription.cancel();
    bloc.dispose();
    bloc.close();
    super.dispose();
  }
}
