import 'package:bloc/bloc.dart';

import 'base_event.dart';
import 'base_state.dart';

export 'package:challenger/presentation/resources/localization/app_localization.dart';
export 'package:rxdart/rxdart.dart';
export 'package:challenger/presentation/scenes/navigator/page_navigator.dart';
export 'package:challenger/presentation/base/base_event.dart';
export 'package:challenger/presentation/base/base_state.dart';
export 'package:challenger/core/utils/validations.dart';

abstract class BaseBloc<Event extends BaseEvent, State extends BaseState>
    extends Bloc<BaseEvent, BaseState> {
  BaseBloc({BaseState initialState}) : super(initialState ?? IdlState());

  void dispose();

  void dispatchEvent(BaseEvent event) {
    assert(event != null);
    // print('dispatchEvent event $event');
    super.add(event);
  }
}
