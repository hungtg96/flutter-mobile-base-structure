import 'package:challenger/presentation/base/base_page_mixin.dart';

class BackgroundHome extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Color(0xFFB1CCE6)
      ..style = PaintingStyle.fill;
    canvas.drawCircle(Offset(0, 0), 350, paint);

    paint.color = Color(0xFFD7ADCD);
    paint.style = PaintingStyle.fill;
    canvas.drawCircle(Offset(400, 0), 200, paint);

    paint.color = Color(0xffF891B8);
    paint.style = PaintingStyle.fill;
    canvas.drawCircle(Offset(400, 250), 200, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
