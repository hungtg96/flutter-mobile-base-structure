import 'dart:ui' show Color;

import 'package:challenger/presentation/base/base_page_mixin.dart';

class CustomBottomNavigationBarItem {
  const CustomBottomNavigationBarItem({
    @required
        this.icon,
    this.label,
    Widget activeIcon,
    this.backgroundColor,
  })  : activeIcon = activeIcon ?? icon,
        assert(label == null),
        assert(icon != null);

  final Widget icon;

  final Widget activeIcon;

  final Widget label;

  final Color backgroundColor;
}
