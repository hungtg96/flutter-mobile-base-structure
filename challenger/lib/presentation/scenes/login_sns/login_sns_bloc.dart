import 'package:challenger/domain/model/home_menu_model.dart';
import 'package:challenger/presentation/base/base_bloc.dart';
import 'package:challenger/presentation/scenes/login_sns/login_sns_state.dart';
import 'package:challenger/presentation/base/base_page_mixin.dart';

import 'login_sns_event.dart';

class LoginSnsBloc extends BaseBloc<BaseEvent, BaseState> {
  @override
  void dispose() {
    // TODO: implement dispose
  }

  @override
  Stream<BaseState> mapEventToState(BaseEvent event) async* {
    switch (event.runtimeType) {
      case SnsFetchingEvent:
        {
          await Future.delayed(const Duration(seconds: 3), () {});
          yield SnsFetchedState(listSns: _getListHomeMenuItem());
          break;
        }
    }
  }

  //dummy data
  List<SnsModelItem> _getListHomeMenuItem() {
    return <SnsModelItem>[
      SnsModelItem(
          icon: "assets/images/icon_sns_line.png",
          title: "LINEアカウントでログイン",
          colors: [Color(0xFF4CAF50), Color(0xFF4CAF50), Color(0xFF4CAF50)]),
      SnsModelItem(
          icon: "assets/images/icon_sns_fb.png",
          title: "Facebookアカウントでログイン",
          colors: [Color(0xFF4E6297), Color(0xFF4E6297), Color(0xFF4E6297)]),
      SnsModelItem(
          icon: "assets/images/icon_sns_insta.png",
          title: "Instagramアカウントでログイン",
          colors: [Color(0xFFEEA054), Color(0xFFD64763), Color(0xFFB62A99)]),
      SnsModelItem(
          icon: "assets/images/icon_sns_twitter.png",
          title: "Twitterアカウントでログイン",
          colors: [Color(0xFF55ACE3), Color(0xFF55ACE3), Color(0xFF55ACE3)]),
      SnsModelItem(
          icon: "assets/images/icon_sns_apple.png",
          title: "Apple ID アカウントでログイン",
          colors: [Color(0xFF5A5A5A), Color(0xFF5A5A5A), Color(0xFF5A5A5A)]),
    ];
  }
}
