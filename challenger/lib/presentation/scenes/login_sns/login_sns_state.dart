import 'package:challenger/presentation/base/base_bloc.dart';
import 'package:challenger/domain/model/home_menu_model.dart';
import 'package:challenger/presentation/base/base_page_mixin.dart';

class SnsFetchedState extends BaseState {
  final List<SnsModelItem> listSns;

  SnsFetchedState({@required this.listSns});
}

class SnsFetchingState extends BaseState {}
