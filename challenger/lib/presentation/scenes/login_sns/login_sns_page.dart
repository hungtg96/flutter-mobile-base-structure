import 'package:challenger/presentation/base/base_bloc.dart';
import 'package:challenger/presentation/base/base_event.dart';
import 'package:challenger/presentation/base/base_page.dart';
import 'package:challenger/presentation/base/base_page_mixin.dart';
import 'package:challenger/presentation/base/base_state.dart';
import 'package:challenger/presentation/scenes/login_sns/login_sns_bloc.dart';
import 'package:challenger/presentation/scenes/login_sns/login_sns_event.dart';
import 'package:challenger/presentation/scenes/login_sns/login_sns_router.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:challenger/presentation/scenes/login_sns/login_sns_state.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoginSnsPage extends BasePage {
  @override
  State<StatefulWidget> createState() => LoginSnsPageState();
}

class LoginSnsPageState
    extends BasePageState<LoginSnsBloc, LoginSnsPage, LoginSnsRouter> {
  @override
  void initState() {
    super.initState();
    this.bloc.dispatchEvent(SnsFetchingEvent());
  }

  @override
  Widget buildBody(BuildContext context, BaseBloc<BaseEvent, BaseState> bloc) {
    return BlocBuilder<LoginSnsBloc, BaseState>(
      cubit: bloc,
      builder: (context, state) {
        if (bloc.state is SnsFetchedState) {
          return Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: _getBackground(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                _quoteView(),
                _listSocialNetwork(bloc.state),
              ],
            ),
          );
        } else {
          return Scaffold(
            body: Container(
              alignment: Alignment.center,
              child: SpinKitFadingCircle(
                color: Color(0xFF90BAE1),
                size: 50.0,
              ),
            ),
          );
        }
      },
    );
  }

  BoxDecoration _getBackground() {
    return BoxDecoration(
        image: DecorationImage(
            image: AssetImage("assets/images/background.png"),
            fit: BoxFit.cover));
  }

  Widget _quoteView() {
    return Container(
      margin: EdgeInsets.only(top: 113),
      child: Column(
        children: [
          Text(
            "ようこそ！",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
                color: Color(0xFF333333)),
            textAlign: TextAlign.center,
          ),
          Container(
            margin: EdgeInsets.only(top: 30),
            child: Text(
              "SNSのアカウントでログイン失敗しました。",
              style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                  color: Color(0xFFEB5757)),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  Widget _listSocialNetwork(SnsFetchedState bloc) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.7,
      child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        itemCount: bloc.listSns.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 64,
            margin: EdgeInsets.only(left: 30, right: 30, bottom: 15),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(60)),
              gradient: LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  colors: bloc.listSns[index].colors),
            ),
            child: Row(
              children: [
                Container(
                    margin: EdgeInsets.only(left: 31, right: 21),
                    child: Image.asset(bloc.listSns[index].icon)),
                Text(
                  bloc.listSns[index].title,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w300),
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
