import 'package:challenger/presentation/base/base_bloc.dart';
import 'package:challenger/presentation/base/base_page_mixin.dart';
import 'package:challenger/domain/model/home_menu_model.dart';

class HomeFetchedState extends BaseState {
  final List<HomeMenuItem> homeMenus;

  HomeFetchedState({@required this.homeMenus});
}

class HomeFetchingState extends BaseState {}