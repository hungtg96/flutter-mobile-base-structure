import 'package:challenger/presentation/base/base_bloc.dart';
import 'package:challenger/presentation/base/base_event.dart';
import 'package:challenger/presentation/base/base_page.dart';
import 'package:challenger/presentation/base/base_page_mixin.dart';
import 'package:challenger/presentation/base/base_state.dart';
import 'package:challenger/presentation/scenes/home/home_bloc.dart';
import 'package:challenger/presentation/scenes/home/home_router.dart';
import 'package:challenger/presentation/widgets/custom_bottom_bar/background.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:challenger/presentation/scenes/home/home_event.dart';

import 'home_state.dart';

class HomePage extends BasePage {
  @override
  State<StatefulWidget> createState() => HomePageState();
}

class HomePageState extends BasePageState<HomeBloc, HomePage, HomeRouter> {
  @override
  void initState() {
    super.initState();
    this.bloc.dispatchEvent(HomeFetchingEvent());
  }

  @override
  Widget buildBody(BuildContext context, BaseBloc<BaseEvent, BaseState> bloc) {
    return BlocBuilder<HomeBloc, BaseState>(
        cubit: bloc,
        builder: (context, state) {
          if (bloc.state is HomeFetchedState) {
            return Scaffold(
              body: Stack(
                children: [
                  _getBackgroundTop(),
                  _getPaneBackground(bloc.state, context),
                  _getHeaderPane(),
                ],
              ),
              bottomNavigationBar: ConvexAppBar(
                style: TabStyle.fixedCircle,
                items: [
                  TabItem(icon: Icons.home, title: 'Home'),
                  TabItem(icon: Icons.favorite, title: 'Favorites'),
                  TabItem(icon: Icons.access_alarm, title: 'Favorites'),
                  TabItem(icon: Icons.search, title: 'Search'),
                  TabItem(icon: Icons.person, title: 'Profile'),
                ],
                color: Color(0xFF999999),
                backgroundColor: Color(0xFFFFFFFF),
                activeColor: Color(0xFF90BAE1),
                initialActiveIndex: 0,
                //optional, defa
                // ult as 0
                onTap: (int i) {
                  //will print index value of which item is been clicked
                  print("index $i");
                },
              ),
            );
          } else {
            return Scaffold(
              body: Container(
                alignment: Alignment.center,
                child: SpinKitFadingCircle(
                  color: Color(0xFF90BAE1),
                  size: 50.0,
                ),
              ),
            );
          }
        });
  }

  Widget _getBackgroundTop() {
    return Container(
      child: CustomPaint(
        painter: BackgroundHome(),
      ),
    );
  }

  Widget _getPaneBackground(HomeFetchedState bloc, BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(topRight: Radius.circular(40)),
            color: Color(0xFFFFFFFF)),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.7,
        child: _getGridPane(bloc),
      ),
    );
  }

  Widget _getGridPane(HomeFetchedState bloc) {
    return Container(
      margin: EdgeInsets.only(top: 30, bottom: 30),
      child: GridView.builder(
          itemCount: bloc.homeMenus.length,
          primary: false,
          padding: const EdgeInsets.all(5),
          gridDelegate:
              SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: [
                Image.asset(bloc.homeMenus[index].icon),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Text(bloc.homeMenus[index].title),
                )
              ],
            );
          }),
    );
  }

  Widget _getHeaderPane() {
    return Stack(
      children: [
        Positioned(
          top: 80,
          left: 30,
          child: Container(
            width: 48.0,
            height: 48.0,
            decoration: new BoxDecoration(
                border: Border.all(color: Color(0xFFFFFFFF), width: 2),
                borderRadius: BorderRadius.circular(20),
                image: new DecorationImage(
                    fit: BoxFit.fill,
                    image: NetworkImage(
                        "https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg.jpg"))),
          ),
        ),
        Positioned(
          top: 85,
          left: 110,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Linkさん、こんにちは。",
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
              Text("ゴールド会員",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 13,
                      fontWeight: FontWeight.normal))
            ],
          ),
        ),
        Positioned(
          child: Stack(
            children: [
              Positioned(
                child: Image.asset(
                  "assets/images/icon_home_noti.png",
                  width: 24,
                  height: 24,
                ),
                top: 90,
                right: 50,
              ),
              Positioned(
                top: 80,
                right: 30,
                child: ConstrainedBox(
                  constraints: BoxConstraints(minWidth: 25),
                  child: Container(
                    child: Text("9+",
                        textAlign: TextAlign.center,
                        style:
                            TextStyle(color: Color(0xFFF04582), fontSize: 13)),
                    decoration: BoxDecoration(
                        color: Color(0xFFFFF7BD),
                        borderRadius: BorderRadius.circular(15)),
                  ),
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
