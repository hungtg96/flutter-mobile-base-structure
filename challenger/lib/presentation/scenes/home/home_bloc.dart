import 'package:challenger/presentation/base/base_bloc.dart';
import 'package:challenger/domain/model/home_menu_model.dart';
import 'package:challenger/presentation/scenes/home/home_event.dart';
import 'home_state.dart';
import 'dart:async';


class HomeBloc extends BaseBloc<BaseEvent, BaseState> {
  @override
  void dispose() {
  }

  @override
  Stream<BaseState> mapEventToState(BaseEvent event) async* {
    if (event is HomeFetchingEvent) {
      await Future.delayed(const Duration(seconds: 3), (){});
      yield HomeFetchedState(homeMenus: _getListHomeMenuItem());
    }
  }

  //dummy data
  List<HomeMenuItem> _getListHomeMenuItem() {
    return <HomeMenuItem>[
      HomeMenuItem(title: "サービス\nメニュー", icon: "assets/images/icon_home_salon.png"),
      HomeMenuItem(title: "シミュレー\nション", icon: "assets/images/icon_home_style.png"),
      HomeMenuItem(title: "スタンプ", icon: "assets/images/icon_home_line_app.png"),
      HomeMenuItem(title: "予約履歴", icon: "assets/images/icon_home_calendar.png"),

      HomeMenuItem(title: "シャンプー", icon: "assets/images/icon_home_love.png"),
      HomeMenuItem(title: "美容室からの\nお知らせ", icon: "assets/images/icon_home_chat.png"),
      HomeMenuItem(title: "EC", icon: "assets/images/icon_home_order.png"),
      HomeMenuItem(title: "お勧めサービス・クーポン", icon: "assets/images/icon_home_discount.png"),

      HomeMenuItem(title: "問合わせ", icon: "assets/images/icon_home_qa.png"),
      HomeMenuItem(title: "カルテ", icon: "assets/images/icon_home_note.png"),
      HomeMenuItem(title: "クチコミ", icon: "assets/images/icon_home_care.png"),
      HomeMenuItem(title: "会員情報", icon: "assets/images/icon_home_user.png"),
    ];
  }
}