import 'package:challenger/presentation/base/base_state.dart';

class InLogingIn extends BaseState {}

class LoginSuccessState extends BaseState {}

class LoginFailureState extends BaseState {}

class ShowPasswordState extends BaseState {
  var isShowPassword = false;
  ShowPasswordState(this.isShowPassword);
}

class OnLogInSnsState extends BaseState {}

class OnRequestForgotPasswordState extends BaseState {}