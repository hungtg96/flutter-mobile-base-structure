import 'package:challenger/presentation/base/base_event.dart';

class LoginSuccessEvent extends BaseEvent {}

class LoginFailureEvent extends BaseEvent {}

class OnRequestLogInEvent extends BaseEvent {}

class OnRequestShowPassWord extends BaseEvent {
  var isShowPassword = false;
  OnRequestShowPassWord(this.isShowPassword);
}

class OnLogInSnsEvent extends BaseEvent {}

class OnRequestForgotPassword extends BaseEvent {}