import 'package:challenger/presentation/base/base_state.dart';
import 'package:flutter/material.dart';
import 'package:challenger/presentation/base/base_router.dart';
import 'package:challenger/presentation/scenes/login/login_state.dart';
import 'package:challenger/presentation/utils/app_sate.dart';
import 'package:challenger/presentation/scenes/home/home_page.dart';
import 'package:challenger/presentation/scenes/login_sns/login_sns_page.dart';

class LoginRouter with BaseRouter {
  @override
  onNavigate({BuildContext context, BaseState state}) {
    switch (state.runtimeType) {
      case LoginFailureState:
        PageNavigator(initTag: AppState.launch)
            .materialPush(context: context, page: HomePage());
        break;
      case LoginSuccessState:
        PageNavigator(initTag: AppState.launch)
            .materialPush(context: context, page: HomePage());
        break;

      case OnLogInSnsState:
        PageNavigator(initTag: AppState.launch)
            .materialPush(context: context, page: LoginSnsPage());
        break;

//      case OnRequestForgotPasswordState:
//          PageNavigator(initTag: AppState.launch)
//              .materialPush(context: context, page: HomePage());
//        break;
    }
  }
}
