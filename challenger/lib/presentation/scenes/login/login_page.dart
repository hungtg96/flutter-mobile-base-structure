import 'package:challenger/presentation/base/base_page_mixin.dart';
import 'package:challenger/presentation/scenes/login/index.dart';
import 'package:flutter/material.dart';
import 'package:challenger/presentation/base/base_bloc.dart';
import 'package:challenger/presentation/base/base_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:challenger/presentation/utils/app_sate.dart';
import 'package:challenger/presentation/scenes/home/home_page.dart';
import 'login_bloc.dart';
import 'login_event.dart';
import 'login_router.dart';

class LoginPage extends BasePage {
  @override
  State<StatefulWidget> createState() => LoginPageState();
}

class LoginPageState extends BasePageState<LoginBloc, LoginPage, LoginRouter> {
  LoginBloc _bloc;
  var isHidePassword = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget buildBody(BuildContext context, BaseBloc bloc) {
    _bloc = bloc;
    return BlocBuilder<LoginBloc, BaseState>(
      cubit: bloc,
      builder: (context, state) {
        if (state is ShowPasswordState) {
          isHidePassword = state.isShowPassword;
        }
        return Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: _getBackground(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              _quoteView(),
              _loginFormView(),
              _getButtonLogin(),
              _authoriseView()
            ],
          ),
        );
      },
    );
  }

  Widget _quoteView() {
    return Container(
      margin: EdgeInsets.only(top: 113),
      child: Text(
        "ようこそ！",
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 30,
            color: Color(0xFF333333)),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _loginFormView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(right: 35, left: 35, top: 114, bottom: 15),
          child: TextField(
            onChanged: (text) {
              _bloc.changeUsername(text);
            },
            decoration: InputDecoration(
                hintText: "メールアドレス",
                hintStyle: TextStyle(color: Color(0xFF4F4F4F), fontSize: 16)),
          ),
        ),
        Container(
          margin: EdgeInsets.only(right: 35, left: 35),
          child: TextField(
            onChanged: (text) {
              _bloc.changePassword(text);
            },
            enableSuggestions: false,
            autocorrect: false,
            obscureText: isHidePassword ? true : false,
            decoration: InputDecoration(
                suffixIcon: IconButton(
                  icon: isHidePassword
                      ? Image.asset("assets/images/icon_hide_password.png")
                      : Image.asset("assets/images/icon_show_password.png"),
                  iconSize: 12,
                  onPressed: () {
                    _bloc.dispatchEvent(OnRequestShowPassWord(!isHidePassword));
                  },
                ),
                hintText: "パスワード",
                hintStyle: TextStyle(color: Color(0xFF4F4F4F), fontSize: 16)),
          ),
        ),
      ],
    );
  }

  Widget _authoriseView() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
          onTap: (){
            _bloc.dispatchEvent(OnLogInSnsEvent());
          },
          child: Container(
              margin: EdgeInsets.only(left: 35, top: 96),
              child: Text(
                "SNSでログイン",
                style: TextStyle(
                    color: Color(0xFFCC7289),
                    fontSize: 18,
                    decoration: TextDecoration.underline),
              )),
        ),
        GestureDetector(
          onTap: (){
            _bloc.dispatchEvent(OnRequestForgotPassword());
          },
          child: Container(
              margin: EdgeInsets.only(right: 35, top: 96),
              child: Text(
                "新規登録",
                style: TextStyle(
                    color: Color(0xFFCC7289),
                    fontSize: 18,
                    decoration: TextDecoration.underline),
              )),
        ),
      ],
    );
  }

  BoxDecoration _getBackground() {
    return BoxDecoration(
        image: DecorationImage(
            image: AssetImage("assets/images/background.png"),
            fit: BoxFit.cover));
  }

  Widget _getButtonLogin() {
    return Container(
      margin: EdgeInsets.only(top: 45),
      child: FlatButton(
        minWidth: 188,
        height: 50,
        color: Color(0xFFEB88B7),
        shape: RoundedRectangleBorder(
            side: BorderSide(color: AppColors.colorButton),
            borderRadius: BorderRadius.circular(55)),
        onPressed: () {
          FocusScope.of(context).requestFocus(FocusNode());
          _bloc.dispatchEvent(OnRequestLogInEvent());
        },
        child: Text(
          "ログイン",
          style: TextStyle(color: AppColors.white, fontSize: 18),
        ),
      ),
    );
  }
}
