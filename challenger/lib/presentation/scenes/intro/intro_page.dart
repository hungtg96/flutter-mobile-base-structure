import 'package:challenger/presentation/base/base_bloc.dart';
import 'package:challenger/presentation/base/base_event.dart';
import 'package:challenger/presentation/base/base_page.dart';
import 'package:challenger/presentation/base/base_page_mixin.dart';
import 'package:challenger/presentation/base/base_state.dart';
import 'package:challenger/presentation/scenes/intro/intro_bloc.dart';
import 'package:challenger/presentation/scenes/login/index.dart';
import 'package:challenger/presentation/utils/app_sate.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'intro_router.dart';

class IntroPage extends BasePage {
  @override
  State<StatefulWidget> createState() => IntroPageState();
}

class IntroPageState extends BasePageState<IntroBloc, IntroPage, IntroRouter> {
  IntroBloc _bloc;
  final _scrollController = ScrollController();
  final policyText =
      "**Privacy Policy**\n\nAbc built the Abc app as a Free app. This SERVICE is provided by Abc at no cost and is intended for use as is.\n\nThis page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our Service.\n\nIf you choose to use our Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that we collect is used for providing and improving the Service. We will not use or share your information with anyone except as described in this Privacy Policy.\n\nThe terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Abc unless otherwise defined in this Privacy Policy.";
  final des =
      "当アプリは、自身の利用する美容室を登録し、つながることに\nより、美容室へ自身の自分らしさを伝え、幸せのための実現を\n目指す支援を行うためのアプリです。\n利用料金は無料です。\nご利用規約を確認し、同意することによりご利用が可能になります。";

  @override
  Widget buildBody(BuildContext context, BaseBloc<BaseEvent, BaseState> bloc) {
    _bloc = bloc;
    return BlocBuilder<IntroBloc, BaseState>(
      cubit: bloc,
      builder: (context, state) {
        return Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: _getBackground(),
          child: Stack(
            children: [_getPolicyView(), _getButtonLogin()],
          ),
        );
      },
    );
  }

  BoxDecoration _getBackground() {
    return BoxDecoration(
        image: DecorationImage(
            image: AssetImage("assets/images/background.png"),
            fit: BoxFit.cover));
  }

  Widget _getButtonLogin() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        margin: EdgeInsets.only(bottom: 70),
        child: FlatButton(
          minWidth: 188,
          height: 50,
          color: Color(0xFFEB88B7),
          shape: RoundedRectangleBorder(
              side: BorderSide(color: AppColors.colorButton),
              borderRadius: BorderRadius.circular(55)),
          onPressed: () {
            PageNavigator(initTag: AppState.launch)
                .materialPush(context: context, page: LoginPage());
          },
          child: Text(
            "同意する",
            style: TextStyle(color: AppColors.white, fontSize: 18),
          ),
        ),
      ),
    );
  }

  Widget _getPolicyView() {
    return Align(
      alignment: Alignment.topCenter,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 120, bottom: 30),
            child: Text(
              des,
              style: TextStyle(color: Color(0xFF333333), fontSize: 11),
            ),
          ),
          Container(
            decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.circular(20)),
            margin: EdgeInsets.only(left: 30, right: 30),
            height: 354,
            width: MediaQuery.of(context).size.width,
            child: Scrollbar(
              radius: Radius.circular(5),
              controller: _scrollController,
              child: SingleChildScrollView(
                controller: _scrollController,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 32, bottom: 30),
                  child: Text(policyText),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
