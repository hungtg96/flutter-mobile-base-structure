import 'package:dartz/dartz.dart';
import 'package:challenger/core/error/failures.dart';
import 'package:challenger/domain/repository/authen_repository.dart';

import 'base_usecase.dart';

abstract class AuthenticationUseCases {
  Future<Either<Failure, bool>> login(String _username, String _password);
}

class AuthenticationUseCaseImpl extends BaseUseCase<bool>
    implements AuthenticationUseCases {
  AuthenticationRepository authenRepo;
  String _username;
  String _password;

  AuthenticationUseCaseImpl(this.authenRepo) : assert(authenRepo != null);

  @override
  Future<Either<Failure, bool>> login(String username, String password) async {
    this._username = username;
    this._password = password;
    return this.execute();
  }

  @override
  Future<bool> main() async {
    await authenRepo.login(this._username, this._password);
    return true;
  }
}
