import 'package:challenger/presentation/base/base_page_mixin.dart';

class HomeMenuItem {
  final String icon;
  final String title;

  HomeMenuItem({@required this.icon, @required this.title});
}

class SnsModelItem {
  final String icon;
  final String title;
  final List<Color> colors;

  SnsModelItem({@required this.icon, @required this.title, @required this.colors});
}