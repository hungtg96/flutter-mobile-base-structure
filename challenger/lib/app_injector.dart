import 'package:challenger/presentation/scenes/home/home_bloc.dart';
import 'package:challenger/presentation/scenes/home/home_router.dart';
import 'package:challenger/presentation/scenes/intro/intro_bloc.dart';
import 'package:challenger/presentation/scenes/intro/intro_router.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:challenger/domain/repository/authen_repository.dart';
import 'core/network/network_status.dart';
import 'data/datasource/remote/authen_api_impl.dart';
import 'data/datasource/remote/interface_api.dart';
import 'data/datasource/authen_cache.dart';
import 'data/repository/authen_repository_impl.dart';
import 'domain/usecases/authentication_usecases.dart';
import 'presentation/scenes/login/index.dart';
import 'presentation/scenes/login_sns/login_sns_bloc.dart';
import 'presentation/scenes/login_sns/login_sns_router.dart';

GetIt injector = GetIt.asNewInstance();

initInjector() {
  //API
  injector.registerFactory<AuthenApi>(() => AuthenApiImpl());

  //Storage
  injector.registerLazySingleton<AuthenCache>(() => AuthenCacheImpl());

  //Usecases
  injector.registerFactory<AuthenticationUseCases>(
      () => AuthenticationUseCaseImpl(injector()));

  //Repositories
  injector.registerFactory<AuthenticationRepository>(
      () => AuthenticationRepositoryImpl(injector(), injector()));

  //Blocs
  injector.registerFactory<LoginBloc>(() => LoginBloc(
        injector(),
      ));
  injector.registerFactory<IntroBloc>(() => IntroBloc());
  injector.registerFactory<HomeBloc>(() => HomeBloc());
  injector.registerFactory<LoginSnsBloc>(() => LoginSnsBloc());

  //Routers
  injector.registerFactory<LoginRouter>(() => LoginRouter());
  injector.registerFactory<IntroRouter>(() => IntroRouter());
  injector.registerFactory<HomeRouter>(() => HomeRouter());
  injector.registerFactory<LoginSnsRouter>(() => LoginSnsRouter());

  //Utils
  injector.registerLazySingleton(() => DataConnectionChecker());
  injector.registerLazySingleton<NetworkStatus>(
      () => NetworkStatusImpl(injector()));
}
